#!/bin/bash

PATH="/bin:/usr/bin:/sbin:${PATH}"
REGEX="^SReclaimable"
MEMINFO="/proc/meminfo"
THRESHOLDKB="50000"

IFS='
'

LINE=($(egrep $REGEX $MEMINFO))
for i in ${LINE[@]}; do
        NUMPRE=$(echo $i | tr -s ' ')
        NUM=$(echo $NUMPRE | cut -d ' ' -f2)
        echo $NUM
        if [ $NUM -gt $THRESHOLDKB ]; then
                sync && sysctl -w vm.drop_caches=3
        fi
done

