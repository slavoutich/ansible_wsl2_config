#!/bin/bash

# https://github.com/Supervisor/initscripts/blob/master/slackware

#!/bin/sh
# /etc/rc.d/rc.supervisord
#
# AUTHOR: Josh Jaques <jjaques@gmail.com>
#
# Start/stop/restart supervisor in slackware.
# Specfically tested in v13.37
#
# To make Supervisor start automatically at boot, make this
# file executable:  chmod 755 /etc/rc.d/rc.supervisord

# Time to wait between stop/start on a restart
SHUTDOWN_TIME=5

# Time to wait after a start before reporting success/fail
STARTUP_TIME=1

# Location of the pid file
PIDFILE=/var/run/supervisord/supervisord.pid

# Config of supervisor
CONFIG=/etc/supervisord.conf

# Daemon to start
DAEMON=supervisord

supervisord_start()
{
    $DAEMON -c $CONFIG -j $PIDFILE
}


supervisord_status()
{
    if [ -f $PIDFILE ]
    then
        pgrep $DAEMON | grep -f $PIDFILE > /dev/null 2>/dev/null
        if [ $? -eq 0 ]
        then
            return 0
        else
            return 1
        fi
    else
        return 1
    fi
}


supervisord_stop()
{
    kill $(cat $PIDFILE)
}


supervisord_status || supervisord_start
