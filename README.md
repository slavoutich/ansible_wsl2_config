# ansible_wsl2_config

Set up WSL2 so it is usable, using Ansible.

## Usage

1. Set up [npiperelay](https://github.com/jstarks/npiperelay) if you want to forward Windows SSH agent to Linux.
1. Tune `setup.yml` (change variables or comment out irrelevant for you stuff).
1. Run `ansible-playbook setup.yml --ask-become-pass`

## What it does
- Sets up supervisor, launches it on login
- Sets up SSH server to ssh from Windows (if needed)
- Sets up a SSH_AUTH_SOCK for Linux with the help of
  [npiperelay](https://github.com/jstarks/npiperelay) and socat
- Sets up cron to drop filesystem caches every five minutes to overcome
  [this bug](https://github.com/microsoft/WSL/issues/4166)

## Tips and tricks

### Setting up X11

1. Set up [VcXsrv](https://sourceforge.net/projects/vcxsrv/) on Windows side
1. Launch XLaunch and save its configuration to a file in arbitrary location (or just take [mine](config.xlaunch))
1. Create a shortcut for XLaunch in autostart folder (`Win+R` -> type `shell:startup`, press Enter) and edit its `Target` (Right click -> `Properties` -> `Shortcut` tab) to look like `"C:\Program Files\VcXsrv\xlaunch.exe" -run "C:\path\to\config.xlaunch"`)
1. Add to `.bashrc` the following string:
  ```
  export DISPLAY=$(cat /etc/resolv.conf | grep nameserver | awk '{print $2}'):0.0
  ```

**IMPORTANT** When you launch XLaunch the first time, you will be prompted by Firewall about the home and public networks communications. You must allow Public networks communications -- the network between WSL2 and Windows seems to be public enough for it.